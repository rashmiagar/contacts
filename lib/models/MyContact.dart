import 'package:equatable/equatable.dart';

class MyContact extends Equatable {
  final String name;
  final List<String> emails;
  final List<String> phones;
  final int avatar;

  MyContact({this.name, this.avatar, this.emails, this.phones});

  @override
  List<Object> get props => [phones.first];
}
