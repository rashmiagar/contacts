import 'dart:io';
import 'package:localstorage/localstorage.dart';
import 'package:path_provider/path_provider.dart';

class ContactsCache {
  static final ContactsCache _instance = ContactsCache._ctor();
  factory ContactsCache() {
    return _instance;
  }
  ContactsCache._ctor();
  LocalStorage localStorage;

  init() async {
    print("called init");
    if (Platform.isIOS) {
      localStorage = new LocalStorage(
          'contacts', getApplicationSupportDirectory().toString());
    } else {
      localStorage = new LocalStorage('contacts');
      print(localStorage);
    }
    await localStorage.ready;
  }

  Future<dynamic> get getStorage async {
    await localStorage.ready;
    return localStorage.getItem('contacts');
  }

  set setStorage(List<String> value) {
    localStorage.setItem('contacts', value);
  }
}
