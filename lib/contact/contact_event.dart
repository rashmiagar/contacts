part of 'contact_bloc.dart';

@immutable
abstract class ContactEvent extends Equatable {
  final MyContact contact;
  const ContactEvent(this.contact);

  @override
  List<Object> get props => [contact.phones.first];
}

class FetchContacts extends ContactEvent {
  final MyContact contact;
  FetchContacts(this.contact) : super(contact);

  @override
  List<Object> get props => [contact.phones.first];
}
