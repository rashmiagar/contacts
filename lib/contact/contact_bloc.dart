import 'package:bloc/bloc.dart';
import 'package:contacts/models/MyContact.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:contacts/contacts_cache.dart';

part 'contact_event.dart';
part 'contact_state.dart';

class ContactBloc extends Bloc<ContactEvent, ContactState> {
  ContactBloc() : super(ContactInitial());

  @override
  Stream<ContactState> mapEventToState(
    ContactEvent event,
  ) async* {
    if (event is FetchContacts) {
      List<String> cacheData = await ContactsCache().getStorage;
      List<String> cachedPhones = [];
      List<String> cachedEmails = [];
      List<String> uncachedEmails = [];
      List<String> uncachedPhones = [];
      for (String phone_num in event.contact.phones) {
        phone_num = phone_num.replaceAll(" ", "");
        if (cacheData.contains(phone_num)) {
          cachedPhones.add(phone_num);
        } else {
          uncachedPhones.add(phone_num);
        }
      }

      for (String email in event.contact.emails) {
        if (cacheData.contains(email)) {
          cachedEmails.add(email);
        } else {
          uncachedEmails.add(email);
        }
      }

      if (cachedPhones.isEmpty && cachedEmails.isEmpty) {
        yield NonCached(
            phones: event.contact.phones, emails: event.contact.emails);
      } else {
        yield Cached(
            phones: cachedPhones,
            emails: cachedEmails,
            uncachedEmails: uncachedEmails,
            uncachedPhones: uncachedPhones);
      }
    }
  }
}
