part of 'contact_bloc.dart';

@immutable
abstract class ContactState {}

class ContactInitial extends ContactState {}

class Cached extends ContactState {
  final List<String> phones, emails, uncachedPhones, uncachedEmails;

  Cached({this.phones, this.emails, this.uncachedPhones, this.uncachedEmails});
}

class NonCached extends ContactState {
  final List<String> phones, emails;

  NonCached({this.phones, this.emails});
}
